package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author Guo Haiqiang
 * @Date 2020/12/2 20:35
 */
@Service
public class SupplierServiceImpl implements SupplierService {

	@Autowired
	private SupplierDao supplierDao;

	@Override
	public ServiceVO getCode() {

		// 获取当前商品最大编码
		String code = supplierDao.getMaxCode();

		// 在现有编码上加1
		Integer intCode = Integer.parseInt(code) + 1;

		// 将编码重新格式化为4位数字符串形式
		String unitCode = intCode.toString();

		for(int i = 4;i > intCode.toString().length();i--){

			unitCode = "0"+unitCode;

		}
		return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
	}

	public Map<String, Object> listSupplier(Integer page, Integer rows, String supplierName) {
		Map<String,Object> map = new HashMap<>();

		page = page == 0 ? 1 : page;
		int offSet = (page - 1) * rows;

		List<Supplier> supplierList= supplierDao.getSupplierList(offSet, rows, supplierName);
		map.put("total", supplierDao.getMaxCode());
		map.put("rows", supplierList);

		return map;
	}

	@Override
	public void save(Supplier supplier) {
		supplierDao.save(supplier);
	}

	@Override
	public void delete(Integer ids) {
		supplierDao.delete(ids);

	}
}
