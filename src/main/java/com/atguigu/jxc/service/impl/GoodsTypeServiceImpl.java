package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.service.GoodsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author Guo Haiqiang
 * @Date 2020/12/4 15:17
 */
@Service
public class GoodsTypeServiceImpl implements GoodsTypeService {


	@Autowired
	GoodsTypeDao goodsTypeDao;
	@Override
	public List<GoodsType> findById(Integer id) {

		return goodsTypeDao.selectList();
	}

	@Override
	public List<GoodsType> findByParentId(int parentId) {
		return goodsTypeDao.findByParentId(parentId);
	}

	@Override
	public void save(GoodsType goodsType) {

	}

	@Override
	public void delete(Integer id) {

	}
}
