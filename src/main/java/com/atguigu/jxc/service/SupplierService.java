package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @Author Guo Haiqiang
 * @Date 2020/12/2 20:28
 */

public interface SupplierService {

	ServiceVO getCode();

	/**
	 * 供应商信息分页展示
	 * @param
	 * @return
	 */
	Map<String, Object> listSupplier(Integer page, Integer rows, String supplierName);

	/**
	 * 修改供应商信息
	 * @param supplier
	 */
	public void save(Supplier supplier);

	/**
	 * 根据id删除供应商
	 * @param ids
	 */
	public void delete(Integer ids);


}
