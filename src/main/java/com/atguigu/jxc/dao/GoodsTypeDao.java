package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 商品类别
 */
@Repository
public interface GoodsTypeDao {

	List<GoodsType> selectList();

	List<GoodsType> findByParentId(int parentId);
}
