package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @description 商品信息
 */
@Repository
public interface GoodsDao {


    String getMaxCode();

    List<Goods> getGoodsInventoryList(@Param("offSet") Integer offSet,
                                      @Param("pageRow") Integer pageRow,
                                      @Param("codeOrName") String codeOrName,
                                      @Param("goodsTypeId") Integer goodsTypeId);

}
